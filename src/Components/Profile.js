import React, {Component} from 'react';
import logo from './logo.svg';
import './profile.css';

class Profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            fullName: 'Fernand Mesange',
            bio: "lorem ipsum",
            imgSrc: logo,
            profession: "Software Engineer",
            show: true,
            secondSinceMounted: 0,
        }
    }

    showProfile = () =>{
        if(this.state.show == true){
            //console.log("unshow profile")
            this.setState({show:false})
            this.componentWillUnmount()
            console.log(this.state.show)
        }else{
            //console.log("show profile")
            this.setState({show:true})
            this.componentDidMount()
            console.log(this.state.show)
        }
    }

    componentDidMount() {
        this.intervalID = setInterval(() => {
            this.setState((prevState) => ({
                secondSinceMounted:prevState.secondSinceMounted + 1
            }));
        },1000);
    }


    componentWillUnmount() {
        clearInterval(this.intervalID);
        this.setState({secondSinceMounted:0})
    }

    
    render(){
        return (
            <div>
                {this.state.show && 
                <>
                <h1>User Profile</h1>
                <h3>FullName :{this.state.fullName}</h3>
                <p>Description : {this.state.bio}</p>
                <img src={this.state.imgSrc} alt="profile" width={100}/>
                <h3>Job: {this.state.profession}</h3>
                </>
                }
                <button onClick={this.showProfile}>
                    {this.state.show? "Hide profile" : "Show profile"}
                </button>
                <p>time since component mounted : {this.state.secondSinceMounted}</p>
            </div>
        )
    }
}

export default Profile;


